from debian
run apt-get update &&\
    apt-get install -y \
      bundler \
      git \
      libsqlite3-dev \
      ruby \
    && apt-get clean
add Gemfile /var/www/histoiredalpaga/current/Gemfile
add Gemfile.lock /var/www/histoiredalpaga/current/Gemfile.lock
run cd /var/www/histoiredalpaga/current && bundle install --without development test
add . /var/www/histoiredalpaga/current/
workdir /var/www/histoiredalpaga/current/
run chmod +x /var/www/histoiredalpaga/current/start
env RAILS_ENV production
run rm -rf /var/www/histoiredalpaga/current/public/system \
    && ln -s /var/www/histoiredalpaga/shared/public/system /var/www/histoiredalpaga/current/public/system
cmd ["/var/www/histoiredalpaga/current/start"]
