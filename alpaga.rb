# encoding: utf-8
require 'bundler'

Bundler.require()

#POST      /foo                -> create new record
#GET       /foo                -> list all records
#PUT       /foo/<pk>           -> update record
#POST      /foo/<pk>           -> update record
#DELETE    /foo/<pk>           -> delete record
#GET       /foo/<pk>           -> view record
#GET       /foo/<pk>/edit_form -> edit record form
#GET       /new/foo            -> create record form

class Alpaga < Sinatra::Base

  configure do
    set :haml, :format => :html5
    Sequel::Model.plugin :timestamps
    set :database, Sequel.connect('sqlite://alpaga.db')
    #Product.plugin :timestamps, :create=>:created_on, :update=>:updated_on


    # define database migrations. pending migrations are run at startup and
    # are guaranteed to run exactly once per database.
    migration "create the products table" do
      database.create_table :products do
        primary_key :id
        text        :title

        index :title, :unique => true
      end
    end
  end

  error do
    'Sorry there was a nasty error - ' + env['sinatra.error'].name
  end

  get '/' do
    haml :index
  end

  get '/collection' do
    @products = Product.order_by(:id)
    haml :produits
  end

  get '/product' do
    redirect '/collection'
  end

  post '/product' do 
    Product.create({
        :title => request[:title]
        })

    redirect '/collection'
  end

  get '/new/product' do
    haml :produit_new
  end

  get '/collection/:id' do 
    @produit = Product.filter(:id => params[:id]).first
    haml :produit
  end
    
  run! if app_file == $0

end

class Content
  attr_accessor :titre, :msg, :menu1, :menu2, :menu3
end

class Product < Sequel::Model
end
