# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def init_user (provider, uid)
  authenticator = Authenticator.where(:provider => provider, :uid => uid).first || Authenticator.create(provider: provider, uid: uid)
  user = user = authenticator.user || User.where(:provider => provider, :uid => uid).first || User.new
  user.admin = true;
  if authenticator.user.nil?
    user.authenticators << authenticator
  end
  user.save
end

# Marion Facebook
init_user('facebook', '100000587661829')

# Thomas gmail
init_user('google_oauth2', '110941640205788210949')

# rebuild smart urls
Produit.find_each(&:save)
