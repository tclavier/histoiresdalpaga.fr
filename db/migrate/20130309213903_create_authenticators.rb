class CreateAuthenticators < ActiveRecord::Migration
  def change
    create_table :authenticators do |t|
      t.string :email
      t.string :name
      t.string :provider
      t.string :uid
      t.string :avatar

      t.timestamps
    end
  end
end
