class AddSlugToProduits < ActiveRecord::Migration
  def change
    add_column :produits, :slug, :string, unique: true
  end
end
