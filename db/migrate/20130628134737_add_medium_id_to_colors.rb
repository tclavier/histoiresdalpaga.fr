class AddMediumIdToColors < ActiveRecord::Migration
  def change
    add_column :colors, :medium_id, :integer
  end
end
