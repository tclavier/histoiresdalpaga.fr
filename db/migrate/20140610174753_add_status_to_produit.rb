class AddStatusToProduit < ActiveRecord::Migration
  def change
    add_column :produits, :status, :boolean
  end
end
