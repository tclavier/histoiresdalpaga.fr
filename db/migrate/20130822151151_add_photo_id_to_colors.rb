class AddPhotoIdToColors < ActiveRecord::Migration
  def change
    add_column :colors, :photo_id, :int
  end
end
