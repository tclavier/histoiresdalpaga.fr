class CreateColorsProduitsTable < ActiveRecord::Migration
  def up
    create_table :colors_produits do |t|
      t.belongs_to :color
      t.belongs_to :produit
    end
  end

  def down
    drop_table :colors_produits
  end
end
