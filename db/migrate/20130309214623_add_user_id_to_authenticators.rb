class AddUserIdToAuthenticators < ActiveRecord::Migration
  def change
    add_column :authenticators, :user_id, :integer
  end
end
