class CreateProduits < ActiveRecord::Migration
  def change
    create_table :produits do |t|
      t.string :ref
      t.text :description

      t.timestamps
    end
  end
end
