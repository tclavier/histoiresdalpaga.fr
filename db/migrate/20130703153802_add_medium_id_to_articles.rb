class AddMediumIdToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :medium_id, :integer
  end
end
