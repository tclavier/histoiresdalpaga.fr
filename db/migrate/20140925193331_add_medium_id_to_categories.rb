class AddMediumIdToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :medium_id, :integer
  end
end
