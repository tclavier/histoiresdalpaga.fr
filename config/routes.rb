Alpaga::Application.routes.draw do
  resources :categories 
  
  get 'catalogue' => "categories#collection"

  get "login" => "static_pages#login"
  get "contact.vcf" => "static_pages#contact_vcf"

  resources :colors

  root :to => 'articles#home'

  resources :media
  resources :produits
  resources :articles
  resources :users

  match '/auth/:provider/callback' => 'sessions#create'
  match '/auth/failure' => 'sessions#failure'
  match '/signout' => 'sessions#destroy', :as => :signout

  match '/edito/:short_url' => 'articles#short_url'
  match '/page/:large_page' => 'articles#large_page'

  match '/collection' => "produits#collection"
end
