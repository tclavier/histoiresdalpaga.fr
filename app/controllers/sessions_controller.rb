class SessionsController < ApplicationController
  def create
    auth = request.env["omniauth.auth"]
    authenticator = Authenticator.where(:provider => auth['provider'], :uid => auth['uid']).first || Authenticator.new
    authenticator.update_with_omniauth(auth)

    user = authenticator.user || if session[:user_id]; User.find(session[:user_id]) end || User.where(:provider => auth['provider'], :uid => auth['uid']).first || User.new

    if authenticator.user.nil? 
      user.authenticators << authenticator
    end

    user.save

    session[:user_id] = user.id
    session[:admin] = user.admin
    redirect_to root_url, :notice => "Bienvenue #{user.name} !"

    #raise request.env["omniauth.auth"].to_yaml
  end

  def destroy
    reset_session
    redirect_to root_url, :notice => 'Revenez vite !'
  end

  def failure
    redirect_to root_url, :alert => "Authentication error: #{params[:message].humanize}"
  end
end
