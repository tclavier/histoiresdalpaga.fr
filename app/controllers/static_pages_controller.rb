class StaticPagesController < ApplicationController
  def login
  end

  def contact_vcf
    render 'contact.vcf', :layout => false, :content_type => "text/x-vcard"
  end
end
