class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :admin?, :user_signed_in?, :current_user
  
  def require_admin
    if session[:user_id].nil?
      flash[:warning] = 'You must be logged.'
      redirect_to root_url
    else 
      if !session[:admin]
        flash[:warning] = 'You must be admin to do that.'
        redirect_to root_url
      end
    end
  end

  def admin?
    if session[:user_id].nil?
      false
    else 
      session[:admin]
    end
  end

  def user_signed_in?
    session[:user_id] || current_user
  end
  
  def current_user
    @current_user ||= User.find_by_id!(session[:user_id])
    rescue ActiveRecord::RecordNotFound
    reset_session
    nil
  end

end
