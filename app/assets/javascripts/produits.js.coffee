# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(document).ready ->
  $('.produit_dans_la_liste').hover(
    -> $(this).addClass('produit_en_avant')
    -> $(this).removeClass('produit_en_avant')
  )
  $('.popup-link').magnificPopup({type: 'image', gallery:{ enabled:true }})
  init_bg()
