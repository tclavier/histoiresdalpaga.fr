class User < ActiveRecord::Base
  attr_accessible :email, :name, :provider, :uid, :avatar, :admin, :authenticators_ids
  has_many :authenticators
  accepts_nested_attributes_for :authenticators, :allow_destroy => true

  def name
    @name || authenticators.first.name
  end

  def avatar
    @avatar || authenticators.first.avatar
  end

end
