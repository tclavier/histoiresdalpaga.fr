class Authenticator < ActiveRecord::Base
  attr_accessible :avatar, :email, :name, :provider, :uid
  belongs_to :user

  def update_with_omniauth(auth)
    self.provider = auth['provider']
    self.uid      = auth['uid']
    if auth['info']
      self.name   = auth['info']['name']  || ""
      self.email  = auth['info']['email'] || ""
      self.avatar = auth['info']['image'] || ""
    end
    save
  end
end
