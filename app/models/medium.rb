class Medium < ActiveRecord::Base
  attr_accessible :tags, :file, :tri
  has_attached_file :file, 
    :styles => { :medium => "800x800>", :thumb => "100x100>"}

  def vignette
    "<img src='#{url(:thumb)}'>".html_safe
  end

  def url(size=:thumb)
    file.url(size)
  end

  class << self # Class methods
    def list_potential_promo(id_produit)
      # liste toutes les photos qui ne sont pas déjà en promo pour un autre produit et qui ne sont pas mise en couleur.
      where("id not in (select promo_id from produits where id != ? and promo_id is not null union select medium_id from colors)", id_produit)
    end

    def list_potential_color(id_color)
      # liste toutes les photos qui ne sont pas déjà mise en couleur
      where("id not in (select medium_id from colors where id != ?) and produit_id is null", id_color)
    end

    def list_potential_category(id_category)
      # liste toutes les photos qui ne sont pas déjà mise en categorie
      #where("id not in (select medium_id from categories where id != ?) and produit_id is null", id_category)
      where("produit_id is null")
    end
  end
end

