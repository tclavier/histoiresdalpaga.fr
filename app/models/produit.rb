class Produit < ActiveRecord::Base
  attr_accessible :description, :ref, :medium_ids, :color_ids, :promo_id, :status, :category_id, :tri
  has_many :medium, :order => :tri
  accepts_nested_attributes_for :medium, :allow_destroy => true
  has_and_belongs_to_many :colors
  belongs_to :promo, :class_name => 'Medium'
  belongs_to :category

  extend FriendlyId
  friendly_id :ref, use: :slugged

  def premiere_photo 
    if medium.first
      medium.first
    else
      Medium.all.first
    end
  end

  def photo_promo
    promo || premiere_photo
  end

  def colors_with_photo
    colors.select { |color| color.photo }
  end

  def toutes_les_photos
    photos = Array.new
    photos.concat(medium)
    photos.concat(colors_with_photo.map {|c| c.photo})
  end
end
