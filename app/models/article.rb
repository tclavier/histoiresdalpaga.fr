class Article < ActiveRecord::Base
  attr_accessible :corps, :titre, :url, :medium_id
  belongs_to :medium
end
