class Color < ActiveRecord::Base
  attr_accessible :name, :medium_id, :photo_id
  belongs_to :medium
  belongs_to :photo, :class_name => 'Medium'

  def vignette
    medium.vignette
  end

  def url(size=:thumb)
    medium.url(size)
  end
end
