class Category < ActiveRecord::Base
  attr_accessible :name, :parent_id, :medium_id
  has_ancestry
  belongs_to :medium

  def vignette
    if medium
      medium.vignette
    else
      Medium.all.first.vignette
    end
  end

  def url(size=:thumb)
    if medium
      medium.url(size)
    else 
      Medium.all.first.url(size)
    end
  end
end
