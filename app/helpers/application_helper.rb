module ApplicationHelper
  def markdown(text)  
    md = text || ""
    @renderer = Redcarpet::Render::HTML.new(no_links: true, hard_wrap: true)
    @markdown = Redcarpet::Markdown.new(@renderer, extensions = {lax_spacing: true})
    @markdown.render(md).html_safe
  end  
end
