module CategoriesHelper

  def nested_categories(categories)
    categories.map do |category, sub_categories|
      content_tag :ul do
        if sub_categories.empty?
          content_tag :li do 
            concat category.name
            concat " "
            concat link_to '<i class="icon-edit"></i> Edit'.html_safe, edit_category_path(category), :class => "btn"
            concat " "
            concat link_to '<i class="icon-trash"></i> Destroy'.html_safe, category, :method => :delete, :data => { :confirm => 'Are you sure?' }, :class => "btn"
          end
        else
          content_tag :li do
            concat category.name
            concat " "
            concat link_to '<i class="icon-edit"></i> Edit'.html_safe, edit_category_path(category), :class => "btn"
            concat " "
            concat link_to '<i class="icon-trash"></i> Destroy'.html_safe, category, :method => :delete, :data => { :confirm => 'Are you sure?' }, :class => "btn"
            concat nested_categories(sub_categories)
          end
        end
      end
    end.join.html_safe
  end

  def name_as_text(category)
    out=""
    for i in 1..category.depth
      out << ":"
    end
    out << " "
    out << category.name
  end

  def category_as_array(categories)
    ret=[]
    categories.map do |category, sub_categories|
      ret << [name_as_text(category), category.id]
      unless sub_categories.empty? 
        category_as_array(sub_categories).each do |line|
          ret << line
        end
      end
    end
    ret
  end
end
